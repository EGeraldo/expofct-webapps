

# Aplicações Web da ExpoFCT 2023: Lista de receitas

Bem-vindos ao tutorial de Aplicações Web da ExpoFCT 2023. Para saberem mais sobre a história das aplicações na internet, vejam [esta apresentação](https://docs.google.com/presentation/d/e/2PACX-1vT8-_om_d3kNeyce6kSQbsDfWmDp0WniqJs_ajwd9lYgYZ1mH2DUcPgONlJ1WXdwwX8CD1_AfTKVWAm/pub?start=false&loop=false&delayms=5000).

## Tutorial

Neste tutorial vamos trabalhar com uma linguagem de programação chamada Javascript
e aprender a usá-la para implementar uma simples aplicação que mostra uma lista de
receitas. Segue as instruções abaixo!

### Passo 1: Preparação

Começa por abrir o endereço `http://localhost:5500` no browser. Se tudo correu
bem, deverás ver uma página para esta atividade. Como podes ver a página está
quase vazia. Nos próximos passos vamos alterar esta página para que apareça a
lista de receitas.

Para perceberes como é criada uma página web, abre o ficheiro `index.html`, que
está dentro da pasta `src`, no editor de texto (Visual Studio Code). Este
ficheiro contém toda a estrutura da página. Para alterares o conteúdo da página
basta alterares este ficheiro.

Experimenta, por exemplo, mudar o título, alterando o texto dentro da tag `<h1>`
na linha 19. Ou a cor de fundo da página, alterando a propriedade
`background-color` na linha 6, para outro valor (e.g. `red`, `green`, etc.). Se
quiseres também podes acrescentar texto na página, criando um novo elemento da
forma `<p> </p>` e colocando o texto que quiseres mostrar dentro dessa tag[^1]. Para
veres as tuas alterações basta fazeres refresh da página no browser.

### Passo 2: Mostrar uma receita

Agora que temos uma página web básica podemos começar a mostrar receitas. Para
mostrarmos uma imagem escrevemos `<img src=url-da-imagem>`, substituindo
`url-da-imagem` pelo link. Experimenta acrescentar as seguintes linhas à tua
página, dentro do elemento `<div class="scene">`, para mostrares a imagem, o
título, e o link para o vídeo de uma receita:

    <img src="https://www.themealdb.com/images/media/meals/quuxsx1511476154.jpg">
    <a href="https://www.youtube.com/watch?v=JceGMNG7rpU">Spanish Tortilla</a>

Consegues ver a imagem? Boa! Agora já podemos começar a criar a nossa lista de imagens.

### Passo 3: Mostrar uma lista de receitas
Já conseguimos mostrar a imagem de uma receita! Perfeito. Agora vamos apresentar
várias receitas na nossa página. Vamos começar por criar a lista de
receitas. Para isso, insere as seguintes linhas no ficheiro `index.html`, dentro
do elemento `<div class="scene">`.

    <ul class="carousel">
    </ul>

E de seguida, dentro da lista que criámos (o elemento `<ul>`), inserimos as imagens uma a uma:

    <li class="carousel__cell"><img src="https://www.themealdb.com/images/media/meals/8x09hy1560460923.jpg"><a href="https://www.youtube.com/watch?v=u_OSIChzuL0">Keleya Zaara</a></li>
    <li class="carousel__cell"><img src="https://www.themealdb.com/images/media/meals/uttrxw1511637813.jpg"><a href="https://www.youtube.com/watch?v=w6TS5J8YRA4">Lancashire hotpot</a></li>
    <li class="carousel__cell"><img src="https://www.themealdb.com/images/media/meals/u55lbp1585564013.jpg"><a href="https://www.youtube.com/watch?v=KfJp-QfrCz4">Stuffed Lamb Tomatoes</a></li>

Agora temos várias receitas na nossa página, mas não parece muito bem organizado.
Vamos alterar o desenho da página para as imagens ficarem bonitas!


### Passo 4: Alterar a disposição da página

Uma das partes essenciais de uma página web é o desenho e disposição dos
elementos. Para definirmos o layout da página, precisamos de acrescentar uma
folha de estilo (*cascade style sheet* (css)). Na pasta da aplicação, podes ver
que existe lá um ficheiro chamado `carousel.css` e outro chamado `carousel.js`.
Estes ficheiro contêm as regras para podermos colocar as imagens num *slideshow*.

O primeiro passo é incluir os ficheiros na página. Para isso, vamos editar o
ficheiro `index.html`. No topo do ficheiro, dentro do elemento `<head>`, vamos
acrescentar as seguinte linhas:

    <link rel="stylesheet" href="carousel.css"/>
    <script src="carousel.js"></script>

Agora só falta acrescentarmos os botões para podermos avançar nas imagens!
Coloca o seguinte código depois da `<div>` onde estão as imagens.

    <div class="controls">
        <button id="previous-button">Previous</button>
        <button id="next-button">Next</button>
        <input type="radio" id="horizontal" name="orientation" value="horizontal" checked><label for="horizontal">Horizontal</label>
        <input type="radio" id="vertical" name="orientation" value="vertical"><label for="vertical">Vertical</label>
    </div>

Neste momento deves ter um *slideshow* com as imagens que selecionámos no passo
anterior para colocar na página.


### Passo 5: Obter a lista de receitas da Internet

Boa, chegaste ao último passo! Uma peça fundamental das aplicações web é a
capacidade de ir buscar dados a outros websites, tal como o Youtube, Facebook,
Wikipedia, etc. Neste passo vamos colocar na nossa app receitas publicadas na
internet. Como há sempre receitas novas a serem publicadas, vamos buscar a
informação em tempo real para que a nossa app esteja sempre atualizada! Para
isso, precisamos de pedir ao servidor que nos dê a lista de receitas e depois
escrevemos o código Javascript necessário para as colocarmos na nossa página.
Vamos colocar o seguinte código no fundo da página, depois da tag `</body>`:

Para escrevermos o código precisamos de criar um novo elemento na nossa página.
Depois da tag `</body>`, vamos abrir uma tag de `<script>`:

    <script type="text/javascript">
    </script>

O código que escrevermos dentro desta tag permite-nos manipular o conteúdo e os
elementos da página (e.g. acrescentar uma imagem, mudar o texto de um botão,
etc.).

A nossa aplicação permite que o utilizador insira um termo de pesquisa para
filtrar receitas. Quando o utilizador clica no botão para pesquisar, o texto que
foi inserido na caixa de pesquisa é colocado no endereço da página para podermos
aceder mais tarde formos buscar a lista de receitas ao servidor. Vamos começar
por escrever o código para obter este valor. Primeiro tentamos extrair o valor
do URL (linhas 2 e 3) e, caso não exista nenhum valor (e.g. quando carregamos a
página pela primeira vez) definimos um que queiramos usar por omissão (linhas 4
e 5).

    function getSearchIngredient() {
        let params = new URLSearchParams(window.location.search);
        let search = params.get('search');
        if (!search) {
            search = 'chicken breast';
        }
        return search;
    }

Agora que sabemos qual o ingrediente principal que queremos usar para a lista de
receitas podemos escrever o código para ir buscar essa lista ao servidor e
apresentá-la na página. Usamos a função que definimos anteriormente para obter o
ingrediente principal (linha 2) e passamos esse valor no pedido que fazemos ao
servidor (linha 3)[^2]. Caso o servidor responda com sucesso ao nosso pedido
(linhas 4 e 5), temos dois casos para analisar:

1. Caso não haja receitas, então usamos uma função para mostrar uma mensagem de erro (linhas 6 e 7)
2. Caso haja receitas, usamos outra função para apresentar as receitas na página
(linhas 8 e 9).

Caso o servidor não responda com sucesso ao pedido, por exemplo porque a ligação
à internet falhou, então apresentamos outra mensagem de erro (linha 12).

    function recipes() {
        let search = getSearchIngredient();
        fetch(`https://themealdb.com/api/json/v1/1/filter.php?i=${search}`)
            .then(response => response.json())
                .then(recipes => {
                    if (!recipes.meals) {
                        displayNotFound(search)
                    } else {
                        displayMeals(recipes.meals)
                    }
                })
            .catch (error => fetchError())
    }

Podes experimentar já o código que escrevemos! Para isso basta acrescentares, no
fim da tag `<script>`, a chamada à função que definimos[^5]:

    recipes()

Deixa este código no fim da tag `<script>`. Vais reparar que o comportamento da
nossa aplicação ainda não está totalmente definido para os 3 casos possíveis.
Por enquanto, o código emite um alerta com o resultado esperado em cada um dos
casos. O que nos falta agora é definirmos o comportamento da nossa aplicação
para cada um desses casos!

Vamos começar pela função `fetchError` que é muito simples: basta colocarmos uma
imagem de erro no sítio próprio da nossa página (o elemento com id `not_found`
no ficheiro `index.html`). Criamos o elemento para a imagem (linha 2), dizemos
qual o URL da imagem que queremos mostrar[^4] (linha 3), e colocamos a imagem no
elemento certo (linha 4)[^6]. Não te esqueças que esta função, assim como as que
vamos definir a seguir, devem aparecer **antes** da chamada à função
`recipes()`.

    function fetchError() {
        var image = document.createElement("img");
        image.src = "https://i.ytimg.com/vi/Oy5oFQkhuew/maxresdefault.jpg";
        document.querySelector('#not_found').appendChild(image);
    }

A função `displayNotFound` é parecida. Também podemos mostrar uma simples imagem
de erro, mas já que temos acesso ao valor que o utilizador usou para pesquisar
(`ingredient`, linha 1), podemos apresentar uma mensagem mais elaborada.
Seguimos a mesma lógica de criar o elemento da imagem (linha 2), definir o URL
da imagem que queremos mostrar (linha 3), e colocar no elemento certo na nossa
página (linha 4). A diferença é que usamos um website de imagens que nos permite
passar texto e obter uma imagem com esse texto por cima. O texto que passamos é
da forma "no *ingredient* recipes found", e colocamos nesse texto o ingrediente
que o utilizador pesquisou (linha 3)[^3].

    function displayNotFound(ingredient) {
        var image = document.createElement("img");
        image.src = `https://cataas.com/cat/says/no%20${ingredient}%20recipes%20found`;
        document.querySelector('#not_found').appendChild(image);
    }

Por último, falta-nos escrever a função que mostra a lista de receitas no
slideshow. Esta função recebe a lista de receitas (tal como aparece no
browser[^2]) e, para cada uma (linha 4), coloca-a no slideshow, à semelhança do
que fizemos no passo 3.

Começamos por criar o elemento para o slide (linhas 5-7). Depois criamos a
imagem (linha 9) e utilizamos o URL que vem na resposta do servidor com a
informação da receita (linha 10). Para mostrarmos o título com um link para o
vídeo no Youtube, criamos o elemento de link (linha 13) e colocamos como texto o
título da receita (linha 14). Como ainda não temos o link do Youtube (para isso
temos de fazer outro pedido!), deixamos o link em branco (linha 15). Agora
podemos colocar estes elementos (a imagem e o link) dentro do slide do carousel
(linhas 17 e 18), e por último acrescentamos o slide ao carousel (linha 19).

Agora que temos o slide pronto, fazemos outro pedido ao servidor para obter o
link do Youtube para esta receita, e alteramos no slide para colocar o URL certo
(linhas 21-23).


    function displayMeals(meals) {
        const carousel = document.querySelector('.carousel');
        carousel.innerHTML = "";
        for (meal of meals) {
            const carousel = document.querySelector('.carousel');
            let display = document.createElement("li");
            display.className = 'carousel__cell';

            let image = document.createElement("img");
            image.src = meal.strMealThumb;

            let recipe_title = document.createElement('a');
            recipe_title.innerHTML = meal.strMeal;
            recipe_title.href = "";

            display.appendChild(image);
            display.appendChild(recipe_title);
            carousel.appendChild(display);

            getRecipeDetails(meal.idMeal).then(mealDetails => {
                recipe_title.href = mealDetails.strYoutube;
            });
        }
        onOrientationChange();
    }

Agora, se fizeres refresh à página verás que já aparecem receitas diferentes, e
podes usar a caixa de pesquisa para procurar por ingrediente (e.g. beef, salmon,
chicken, white flour, etc.).

[^1]: No Visual Studio Code podes usar o atalho `Shift-Alt-F` para formatar o documento e organizar o texto no editor.
[^2]: Para veres a resposta do servidor diretamente no teu browser, navega para [este](https://themealdb.com/api/json/v1/1/filter.php?i=chicken) endereço.
[^3]: Para veres a imagem diretamente no teu browser, navega para [este](https://cataas.com/cat/says/no%20sushi%20recipes%20found) endereço.
[^4]: Se quiseres podes usar uma imagem diferente, basta trocares o URL da imagem.
[^5]: Também podes executar este código na consola do browser.
[^6]: Repara que este erro raramente aparecerá, por isso é normal que não vejas esta mensagem durante a atividade.
