var isHorizontal = true;
var radius = 0;
var theta = 0;
var selectedIndex = 0;
var carousel;

function init() {
    const prevButton = document.getElementById('previous-button');
    prevButton.addEventListener('click', function () {
        selectedIndex--;
        rotateCarousel();
    });

    const nextButton = document.getElementById('next-button');
    nextButton.addEventListener('click', function () {
        selectedIndex++;
        rotateCarousel();
    });

    const orientationRadios = document.querySelectorAll('input[name="orientation"]');
    for (var i = 0; i < orientationRadios.length; i++) {
        var radio = orientationRadios[i];
        radio.addEventListener('change', onOrientationChange);
    }

    changeCarousel();
}

function rotateFn(isHorizontal) { return isHorizontal ? 'rotateY' : 'rotateX'; }

function rotateStyle(radius, isHorizontal, angle) {
    const s = `translateZ(${-radius}px) ${rotateFn(isHorizontal)}(${angle}deg) translateZ(${radius}px)`;
    return s
}

function rotateCarousel() {
    var angle = theta * selectedIndex * -1;
    const carousel = document.querySelector('.carousel');
    carousel.style.transform = rotateStyle(radius, isHorizontal, angle);
}


function changeCarousel() {
    const carousel = document.querySelector('.carousel');
    const cells = carousel.querySelectorAll('.carousel__cell');
    const cellWidth = carousel.offsetWidth;
    const cellHeight = carousel.offsetHeight;
    const cellCount = cells.length;
    theta = 360 / cellCount;
    var cellSize = isHorizontal ? cellWidth : cellHeight;
    radius = Math.round( ( cellSize / 2) / Math.tan( Math.PI / cellCount ) );
    for ( var i=0; i < cells.length; i++ ) {
        var cell = cells[i];
        if ( i < cellCount ) {
            // visible cell
            cell.style.opacity = 1;
            var cellAngle = theta * i;
            console.log(cellAngle);
            cell.style.transform = rotateStyle(radius, isHorizontal, cellAngle);
        } else {
            // hidden cell
            cell.style.opacity = 0;
            cell.style.transform = 'none';
        }
    }

    rotateCarousel();
}


function onOrientationChange() {
  var checkedRadio = document.querySelector('input[name="orientation"]:checked');
  isHorizontal = checkedRadio.value == 'horizontal';
  changeCarousel();
}

window.onload = init
