async function getRecipeDetails(id) {
    response = await fetch(`https://themealdb.com/api/json/v1/1/lookup.php?i=${id}`).then(r=>r.json());
    return response.meals[0];
}

function fetchError() {
    alert('Could not make request')
}

function displayNotFound(ingredient) {
    alert(`Could not find any recipe with ${ingredient}`)
}

function displayMeals(meals) {
    alert(`Here are the recipes ${JSON.stringify(meals)}`)
}